import { useState, useEffect,useContext } from 'react'
// import courseData from '../data/courses'
// import CourseCard from '../components/CourseCard'
import UserView from "../components/UserView"
import AdminView from "../components/AdminView"
import UserContext from "../UserContext"


export default function Courses(){

	const [coursesData, setCoursesData] = useState([])
	const { user } = useContext(UserContext)

	const fetchData = () => {
		fetch(`${process.env.REACT_APP_API_URL}/courses`)
		.then(res => res.json())
		.then(data => {
			setCoursesData(data)
		})
	}

	useEffect(() => {
		fetchData()
	}, [])

	return(
		user.isAdmin ? <AdminView coursesProp={coursesData} fetchData={fetchData}/> : <UserView coursesProp={coursesData}/>

		)
		
}