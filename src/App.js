import { useState, useEffect } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Container } from 'react-bootstrap'
import AppNavbar from './components/AppNavbar'
import Home from './pages/Home'
import Courses from './pages/Courses'
import SpecificCourse from './pages/SpecificCourse'
import Register from './pages/Register'
import Login from './pages/Login'
import Error from './pages/Error'
import { UserProvider } from "./UserContext"
import './App.css';

// App.js is typically called the "top-level component"

function App() {

// main app-wide user state
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()

    setUser({
      id: null,
      isAdmin: null
    })
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !== "undefined"){
        setUser({
        id: data._id,
        isAdmin: data.isAdmin
        })
      }else{
        setUser({
        id: null,
        isAdmin: null
        })
      }
    })

  }, [])


  return (
  <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
      <AppNavbar/>
      <Container>
      <Switch>
        <Route exact path="/" component={Home}/>
        <Route exact path="/courses" component={Courses}/>
        <Route exact path="/courses/:courseId" component={SpecificCourse}/>       
        <Route exact path="/register" component={Register}/>
        <Route exact path="/login" component={Login}/>
        <Route component={Error}/>
      </Switch>
      </Container>
    </Router>
  </UserProvider>
  );
}

export default App;
