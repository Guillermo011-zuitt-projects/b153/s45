import React from 'react'

// create a context object
// a context object is a type of object that can be used to store information that can be shared to all other components with the app

const UserContext = React.createContext()

// define the Provider componenet, which distributes states across all our components

export const UserProvider = UserContext.Provider

export default UserContext