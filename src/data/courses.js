

let courseData = [
	{
		id: "wdc001",
		name: "Javascript",
		description: "Lorem ipsum dolor sit amet consectetur, adipisicing, elit. Ex officia magni vel saepe et vero, distinctio iusto doloremque, asperiores labore laboriosam quibusdam consectetur nam consequuntur nulla? Quidem consectetur, ducimus doloribus?",
		price: 1000,
		onOffer: true,
	},
	{
		id: "wdc002",
		name: "CSS",
		description: "Lorem ipsum dolor sit amet consectetur, adipisicing, elit. Ex officia magni vel saepe et vero, distinctio iusto doloremque, asperiores labore laboriosam quibusdam consectetur nam consequuntur nulla? Quidem consectetur, ducimus doloribus?",
		price: 2000,
		onOffer: true,
	},
	{
		id: "wdc003",
		name: "HTML",
		description: "Lorem ipsum dolor sit amet consectetur, adipisicing, elit. Ex officia magni vel saepe et vero, distinctio iusto doloremque, asperiores labore laboriosam quibusdam consectetur nam consequuntur nulla? Quidem consectetur, ducimus doloribus?",
		price: 3000,
		onOffer: true,
	},
	{
		id: "wdc004",
		name: "React",
		description: "Lorem ipsum dolor sit amet consectetur, adipisicing, elit. Ex officia magni vel saepe et vero, distinctio iusto doloremque, asperiores labore laboriosam quibusdam consectetur nam consequuntur nulla? Quidem consectetur, ducimus doloribus?",
		price: 4000,
		onOffer: false,
	}
]

export default courseData