import { Link } from 'react-router-dom'
import { Card } from 'react-bootstrap'


export default function CourseCard({courseProp}){

	const { _id, name, description, price } = courseProp

	// const [seat, setSeat] = useState(30)
	// const [count, setCount] = useState(0)
	// const [isOpen, setIsOpen] = useState(true)

/*
if the array is empty, useEffect will only happen on component mount (when it first loads)
*/
	// useEffect(() => {
	// 	if(seat === 0){
	// 		console.log("Hello")
	// 		setIsOpen(false)
	// 	}
	// }, [seat])

	// function enroll(){
	// 		setSeat(seat - 1)
	// 		setCount(count + 1)
	// }



	return(
				<Card className="cardHighlight m-3">
				<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>					
					<Card.Text>{price}</Card.Text>
			        <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
				</Card.Body>
				</Card>

		)
}