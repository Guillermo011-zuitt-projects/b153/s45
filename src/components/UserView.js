import { useState, useEffect } from 'react'
// import courseData from '../data/courses'
import CourseCard from './CourseCard'


export default function UserView({coursesProp}){

	const [coursesArr, setCoursesArr] = useState([])

	useEffect(() => {
	const courses = coursesProp.map(course => {
		// console.log(courses)
		if(course.isActive){
		return <CourseCard key={course._id} courseProp={course}/>
		}else{
		return null
		}
	})
	setCoursesArr(courses)
	}, [coursesProp])



	return(
		<>
			<h3 className="text-center my-3">Courses</h3>
			{coursesArr}
		</>
		)
		
}