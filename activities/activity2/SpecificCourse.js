import { useParams } from 'react-router-dom'
import { useState, useEffect } from 'react'
import { Card, Button } from 'react-bootstrap'


export default function SpecificCourse(){
// useParams() contains any values we are trying to pass in the URL stored as a ke/value pair
	const { courseId } = useParams()
	console.log(courseId)

	const [courseData, setCourseData] = useState()

	const fetchData = () => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			setCourseData(data)
		})
	}

	useEffect(() => {
		fetchData()
	})


	return(
		<Card className="mt-3">
			<Card.Header className="bg-dark text-white text-center pb-0">
				<h4>{courseData.name}</h4>
			</Card.Header>	
			<Card.Body>
				<Card.Text> {courseData.Description}	</Card.Text>
				<h6> Price: {courseData.price}</h6>
			</Card.Body>
			<Card.Footer>
				<Button variant="primary" block> Enroll </Button>
			</Card.Footer>
		</Card>
		)
}