/*
Activity:
- Create a login page for our React application with the ff. specification:
1. Import all necessary packages and hooks (useState, useEffect, Form, etc)
2. Create a form with an email and password input
3. Make sure to add form validation that checks if the email and password inputs are blank
4. If either field is blank, show a disabled button. If not, show an enabled button.
5. Create a function called loginUser that sends a fetch request to the appropriate API Endpoint that includes a request body containing the email and password inputs' value.
6. When the form is submitted, the loginUser function is called 
7. log the API.server response in the console
8. if the response from the API/server is true (meaning login is successful), show a sweetalert with a success icon that says "Login successful - Welcome to Zuitt!"
9. if the response from the API/server is true (meaning login is successful), show a sweetalert with a success icon that says "Login successful - Welcome to Zuitt!"
*/

import { useState, useEffect }from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function Login(){
	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [isActive, setIsActive] = useState(false)

	const loginUser = (e) => {
		e.preventDefault()
			fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					email: email,
					password: password
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)

				if(data){
							Swal.fire({
							title: "Login Successful",
							icon: "success",
							text: "Welcome to Zuitt"
						})
				}else{
							Swal.fire({
							title: "Authentication failed",
							icon: "error",
							text: "Check your login details and try again"
						})
				}

			})






	}




		useEffect(() => {
			if(email !== '' && password !== ''){
					setIsActive(true)
			}else{
					setIsActive(false)
			}

		}, [email, password])


	return(
		<Form onSubmit={e => loginUser(e)} className="my-4">

			<Form.Group controlId="email">
				<Form.Label>User Email</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter Email"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password" className="mb-4">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter password"
					value={password}
					onChange={e => setPassword(e.target.value)}
					required
				/>				
			</Form.Group>

			{isActive ? <Button variant="primary" type="submit" id="submitBtn">Submit</Button> : <Button variant="secondary" id="submitBtn" disabled>Submit</Button>}
		</Form>

		)


}